/**
 * @author Simon Urli <simon@the6thscreen.fr>
 */

class CMSRepository extends ModelItf {

	private static UserModel = {
		getTableName : function() {
			return "Users"
		},

		fromJSONObject : function (object) {
			return object;
		}
	};

	/**
	 * Name of the repository
	 */
	private _name : string;

	private _users : Array<any>;

	private _users_loaded : boolean;

	constructor(name : string = "", id : number = null, complete : boolean = false, createdAt : string = null, updatedAt : string = null) {
		super(id, complete, createdAt, updatedAt);
		this.setName(name);

		this._users = new Array<any>();
		this._users_loaded = false;
	}

	setName(name : string) {
		this._name = name;
	}

	name() : string {
		return this._name;
	}

	users() : Array<any> {
		return this._users;
	}

	loadUsers(successCallback : Function, failCallback : Function) {
		var self = this;
		var successLoad = function (data) {
			data.forEach( function (object) {
				object.getId = function () {
					return object.id;
				};
			});

			self._users = data;
			self._users_loaded = true;

			successCallback();
		};

		var fail = function (error) {
			failCallback(error);
		};

		this.getAssociatedObjects(CMSRepository, CMSRepository.UserModel, successLoad, fail);
	}

	addUser(userId, successCallback : Function, failCallback : Function) {
		this.associateObject(CMSRepository, CMSRepository.UserModel, userId, successCallback, failCallback);
	}

	removeUser(userId, successCallback : Function, failCallback : Function) {
		this.deleteObjectAssociation(CMSRepository, CMSRepository.UserModel, userId, successCallback, failCallback);
	}

	/**
	 * Load all the lazy loading properties of the object.
	 * Useful when you want to get a complete object.
	 *
	 * @method loadAssociations
	 * @param {Function} successCallback - The callback function when success.
	 * @param {Function} failCallback - The callback function when fail.
	 */
	loadAssociations(successCallback : Function, failCallback : Function) {
		var self = this;

		var success : Function = function(models) {
			if(self._users_loaded) {
				if (successCallback != null) {
					successCallback();
				} // else //Nothing to do ?
			}
		};

		var fail : Function = function(error) {
			if(failCallback != null) {
				failCallback(error);
			} else {
				Logger.error(JSON.stringify(error));
			}
		};

		this.loadUsers(success, fail);
	}

	toJSONObject() {
		return {
			"id": this.getId(),
			"name": this.name()
		};
	}

	/**
	 * Return a User instance as a JSON Object including associated object.
	 * However the method should not be recursive due to cycle in the model.
	 *
	 * @method toCompleteJSONObject
	 * @param {Function} successCallback - The callback function when success.
	 * @param {Function} failCallback - The callback function when fail.
	 */
	toCompleteJSONObject(successCallback : Function, failCallback : Function, onlyId : boolean = false) {
		var self = this;

		var success : Function = function() {
			var data = self.toJSONObject();
			data["users"] = self.serializeArray(self.users(), true);

			successCallback(data);
		};

		var fail : Function = function(error) {
			failCallback(error);
		};

		this.loadAssociations(success, fail);
	}

	/**
	 * Create model in database.
	 *
	 * @method create
	 * @param {Function} successCallback - The callback function when success.
	 * @param {Function} failCallback - The callback function when fail.
	 * @param {number} attemptNumber - The attempt number.
	 */
	create(successCallback : Function, failCallback : Function, attemptNumber : number = 0) {
		this.createObject(CMSRepository, this.toJSONObject(), successCallback, failCallback);
	}

	/**
	 * Retrieve model description from database and create model instance.
	 *
	 * @method read
	 * @static
	 * @param {number} id - The model instance's id.
	 * @param {Function} successCallback - The callback function when success.
	 * @param {Function} failCallback - The callback function when fail.
	 * @param {number} attemptNumber - The attempt number.
	 */
	static read(id : number, successCallback : Function, failCallback : Function, attemptNumber : number = 0) {
		ModelItf.readObject(CMSRepository, id, successCallback, failCallback, attemptNumber);
	}

	/**
	 * Update in database the model with current id.
	 *
	 * @method update
	 * @param {Function} successCallback - The callback function when success.
	 * @param {Function} failCallback - The callback function when fail.
	 * @param {number} attemptNumber - The attempt number.
	 */
	update(successCallback : Function, failCallback : Function, attemptNumber : number = 0) {
		return this.updateObject(CMSRepository, this.toJSONObject(), successCallback, failCallback, attemptNumber);
	}

	/**
	 * Delete in database the model with current id.
	 *
	 * @method delete
	 * @param {Function} successCallback - The callback function when success.
	 * @param {Function} failCallback - The callback function when fail.
	 * @param {number} attemptNumber - The attempt number.
	 */
	delete(successCallback : Function, failCallback : Function, attemptNumber : number = 0) {
		return ModelItf.deleteObject(CMSRepository, this.getId(), successCallback, failCallback, attemptNumber);
	}

	/**
	 * Retrieve all models from database and create corresponding model instances.
	 *
	 * @method all
	 * @param {Function} successCallback - The callback function when success.
	 * @param {Function} failCallback - The callback function when fail.
	 * @param {number} attemptNumber - The attempt number.
	 */
	static all(successCallback : Function, failCallback : Function, attemptNumber : number = 0) {
		return this.allObjects(CMSRepository, successCallback, failCallback, attemptNumber);
	}

	static fromJSONObject(data : any) : CMSRepository {
		return new CMSRepository(data.name, data.id, data.complete, data.createdAt, data.updatedAt);
	}

	/**
	 * Retrieve DataBase Table Name.
	 *
	 * @method getTableName
	 * @return {string} The DataBase Table Name corresponding to Model.
	 */
	static getTableName() : string {
		return "CMSRepositories";
	}
}