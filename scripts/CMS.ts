/**
 * @author Simon Urli <simon@the6thscreen.fr>
 */

/// <reference path="../t6s-core/core-backend/scripts/server/SourceServer.ts" />
/// <reference path="../t6s-core/core-backend/scripts/Logger.ts" />

/// <reference path="./model/ModelItf.ts" />
/// <reference path="./model/CMSRepository.ts" />
/// <reference path="./namespacemanager/ShareNamespaceManager.ts" />
/// <reference path="./namespacemanager/AdminNamespaceManager.ts" />

/**
 * Represents the The 6th Screen Photobox Service.
 *
 * @class Notifier
 * @extends SourceServer
 */
class CMS extends Server {

	/**
	 * Constructor.
	 *
	 * @param {number} listeningPort - Server's listening port..
	 * @param {Array<string>} arguments - Server's command line arguments.
	 */
	constructor(listeningPort : number, arguments : Array<string>) {
		super(listeningPort, arguments);

		this.init();
	}

	/**
	 * Method to init the Notifier server.
	 *
	 * @method init
	 */
	init() {
		var self = this;
		super.addNamespace("admin", AdminNamespaceManager);
	}
}

/**
 * Server's Photobox listening port.
 *
 * @property _PhotoboxListeningPort
 * @type number
 * @private
 */
var _CMSListeningPort : number = process.env.PORT || 6010;

/**
 * Server's Photobox command line arguments.
 *
 * @property _PhotoboxArguments
 * @type Array<string>
 * @private
 */
var _CMSArguments : Array<string> = process.argv;

var serverInstance = new CMS(_CMSListeningPort, _CMSArguments);
serverInstance.run();