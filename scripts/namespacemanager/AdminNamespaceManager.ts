/**
 * @author Simon Urli <simon@the6thscreen.fr>
 */

/// <reference path="../model/CMSRepository.ts" />

class AdminNamespaceManager extends ShareNamespaceManager {

	constructor (socket : any) {
		super(socket);
		var self = this;

		this.addListenerToSocket("RetrieveCMSRepository", function (data) { self.sendObjectDescriptionFromJSONDescriptionWithID(CMSRepository, "CMSRepositoryId", data, "AnswerRetrieveCMSRepository"); });

		this.addListenerToSocket("CreateCMSRepository", function (data) { self.createObject(CMSRepository, data, "AnswerCreateCMSRepository"); });

		this.addListenerToSocket("UpdateCMSRepository", function (data) { self.updateObjectAttribute(CMSRepository, data, "AnswerUpdateCMSRepository"); });
	}

	/**
	 * Retrieve an object of the defined modelClass from the ID given in jsonDescription under the propertyName. Send it back through the channelResponse.
	 * It is possible to specify to return only IDs for associated objects.
	 *
	 * @method sendObjectDescriptionFromJSONDescriptionWithID
	 * @param modelClass - The model for the object to return.
	 * @param propertyName - The property name of the ID to retrieve from the jsonDescription.
	 * @param jsonDescription - A JSON containing the ID of the object to retrieve.
	 * @param channelResponse - The channel to return the object.
	 * @param onlyId - If true it only returns IDs for associated objects. It is false by defaults (complete objects are returned).
	 */
	sendObjectDescriptionFromJSONDescriptionWithID(modelClass : any, propertyName : string, jsonDescription : any, channelResponse : string, onlyId : boolean = false) {
		var self = this;
		var objectId = jsonDescription[propertyName];
		self.sendObjectDescriptionFromId(modelClass, objectId, channelResponse, onlyId);
	}
}